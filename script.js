'use strict'

const allButtons = document.querySelectorAll('button')


document.addEventListener('keydown', (event) => {
    allButtons.forEach((button) => {

        if (event.key.toLowerCase() === button.textContent.toLowerCase()) {
            button.style.backgroundColor = "blue";
        } else {
            button.style.backgroundColor = "#000000";
        }
    })
});
